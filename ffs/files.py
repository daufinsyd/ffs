import yaml
import os

from ffs import path_to_base

conf_file = os.path.join(path_to_base, "ffs.conf")

def read_conf():
    """
    Read and return configuration
    raise error if conf file does not exist
    """
    conf = {"default": {"remote": {"host": "", "user": "", "path": "", "profile": ""}}}
    if os.path.exists(conf_file):
        with open(conf_file, 'r') as f:
            conf = yaml.safe_load(f)  # todo add a try catch ?
    else:
        #raise FileNotFound
        print("File not found")

    return conf


def write_conf(conf):
    """
    Write conf to the file
    """
    exit_code = 1
    try:
        with open(conf_file, 'w') as f:
            f.write(yaml.dump(conf))
        exit_code = 0
    except Exception as e:
        logger.exception("Failed to write conf")

    return exit_code

