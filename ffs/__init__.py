import sys
import os
import faulthandler
import logging

name = "ffs"
version = "0.1"

### PATHs ###
path_to_base = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..")

### LOGGING ###
LOGGING_LEVEL = logging.INFO

logger = logging.getLogger(__name__)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

logging.basicConfig(filename=path_to_base + "/ffs.log", level=LOGGING_LEVEL, format='%(levelname)s:%(asctime)s %(message)s')

faulthandler.enable()
