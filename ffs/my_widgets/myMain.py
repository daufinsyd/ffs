# -*- coding: utf-8 -*-

from PyQt5 import uic
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt as Qt
from ffs.py_ui.main import *

from ffs import files
from ffs import logic
from ffs import logger
import ffs

class MyMain(QtWidgets.QMainWindow, Ui_FFS_Main):
    def __init__(self, parent=None):
        super(MyMain, self).__init__(parent)
        self.init_data()
        self.init_ui()

        self.threads = []

        if logic.check_remote_conf(self.conf["default"]["remote"]):
            # Auto load data only if posible
            self.retreive_data()

    def init_data(self):
        self.conf: str = None
        self.conf = files.read_conf()
        self.remote_files: dict = None

    def init_ui(self):
        self.setupUi(self)
        self.fill_data()

    def retreive_data(self):
        if not self.threads:
            if logic.check_remote_conf(self.conf["default"]["remote"]):
                # check conf
                self.label_status_value.setText("connexion ...")
                retreiver = RetreiveThread(self.conf["default"]["remote"])
                retreiver.data_retreived.connect(self.on_data_retreived)
                self.threads.append(retreiver)
                retreiver.start()
            else:
                logger.warning("No valid conf found")
                QtWidgets.QMessageBox.information(self, "Champs de connexion invalides", "Les champs de " + 
                        "connexion saisis sont incomplet.\nVeillez à saisir au minimum : l'hôte, " + 
                        "l'utilisateur et  un chemin")
        else:
            QtWidgets.QMessageBox.information(self, "Rafraîchissement en cours", "Un rafraîchissement est déjà en cours. Veuillez attendre la fin de ce dernier avant d'en relancer un nouveau.")
        #logic.get_remote_files(self.conf["default"]["remote"])

    # GETTERS #
    def get_hostname(self):
        return self.lineEdit_host_value.text()

    def get_remote_user(self):
        return self.lineEdit_user_value.text()

    def get_remote_path(self):
        return self.lineEdit_path_value.text()

    def get_ff_profile(self):
        return self.lineEdit_profile_value.text()

    # Events #
    def on_data_retreived(self, data):
        logger.debug("Data retreived")
        self.threads = []
        if isinstance(data, dict) and data:
            self.remote_files = data
            self.fill_treeWidget()
            self.label_status_value.setText("connecté")
        else:
            self.label_status_value.setText("erreur")
            QtWidgets.QMessageBox.critical(self, "Échec de la connexion", "Impossible de se connecter au serveur :\n" + str(data))

    def on_buttonBox_accepted(self):
        self.save_ffs_profile()
        self.close()

    def on_buttonBox_rejected(self):
        self.close()

    def on_lineEdit_host_value_textEdited(self):
        self.conf["default"]["remote"]['host'] = self.lineEdit_host_value.text()

    def on_lineEdit_path_value_textEdited(self):
        self.conf["default"]["remote"]['path'] = self.lineEdit_path_value.text()

    def on_lineEdit_profile_value_textEdited(self):
        self.conf["default"]["remote"]['profile'] = self.lineEdit_profile_value.text()

    def on_lineEdit_user_value_textEdited(self):
        self.conf["default"]["remote"]['user'] = self.lineEdit_user_value.text()

    def on_pushButton_refresh_pressed(self):
        self.clear_out_treeWidget()
        self.retreive_data()

    def on_pushButton_restore_pressed(self):
        item = self.treeWidget_saves.currentItem()
        if item:
            file_name = item.data(0, Qt.UserRole)
            if QtWidgets.QMessageBox.question(self, "Restauration", "Voulez-vous revenir à la version : "
                    + str(file_name)) == QtWidgets.QMessageBox.Yes:
                print("OK :)")
                if logic.is_ff_running():
                    QtWidgets.QMessageBox.warning(self, "Firefox est en cours d'exécution", "Il semblerait" + 
                    " que firefox soit en cours d'exécution. Veuillez d'abord fermer toutes les instances de" +
                    " firefox de votre compte avant de continuer.")
                logic.restore_remote(self.conf["default"]["remote"], file_name)
            else:
                print("Cancelled :)")
        else:
            QtWidgets.QMessageBox.information(self, "Aucune sélection", "Veuillez d'abord sélectionner le fichier à restaurer")
        print("restore")

    def on_pushButton_help_pressed(self):
        QtWidgets.QMessageBox.information(self, "Aide", "FirefoxSync, version " + str(ffs.version))

    # Functions #
    def clear_out_treeWidget(self):
        self.treeWidget_saves.clear()

    def fill_data(self):
        self.lineEdit_host_value.setText(self.conf["default"]["remote"]["host"])
        self.lineEdit_user_value.setText(self.conf["default"]["remote"]["user"])
        self.lineEdit_path_value.setText(self.conf["default"]["remote"]["path"])
        self.lineEdit_profile_value.setText(self.conf["default"]["remote"]["profile"])

        #self.fill_treeWidget()

    def fill_treeWidget(self):
        self.clear_out_treeWidget()
        if self.remote_files:
            for k_y,v_y in self.remote_files.items():
                item_y = QtWidgets.QTreeWidgetItem(self.treeWidget_saves)
                item_y.setText(0, k_y)
                item_y.setFlags(Qt.ItemIsEnabled)  # prevent user from selecting this item
                if v_y:
                    for k_m, v_m in v_y.items():
                        item_m = QtWidgets.QTreeWidgetItem(item_y)
                        item_m.setText(0, k_m)
                        item_m.setFlags(Qt.ItemIsEnabled)
                        if v_m:
                            for k_d, v_d in v_m.items():
                                item_d = QtWidgets.QTreeWidgetItem(item_m)
                                item_d.setText(0, k_d)
                                item_d.setFlags(Qt.ItemIsEnabled)
                                if v_d:
                                    for k, v in v_d.items():
                                        # Items
                                        item = QtWidgets.QTreeWidgetItem(item_d)
                                        item.setText(0, k)
                                        item.setData(0, Qt.UserRole, v)




    def save_ffs_profile(self):
        """
        Save hostname, ...
        """
        self.conf["default"]["remote"]["host"] = self.get_hostname()
        self.conf["default"]["remote"]["user"] = self.get_remote_user()
        self.conf["default"]["remote"]["path"] = self.get_remote_path()
        self.conf["default"]["remote"]["profile"] = self.get_ff_profile()
        files.write_conf(self.conf)


class RetreiveThread(QtCore.QThread):
    data_retreived = QtCore.pyqtSignal(object)

    def __init__(self, conf):
        QtCore.QThread.__init__(self)
        self.conf = conf

    def run(self):
        data = logic.get_remote_files(self.conf)
        self.data_retreived.emit(data)
