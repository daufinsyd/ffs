#/usr/bin/python3
# coding=utf-8

import sys
from ffs.my_widgets import myMain

# PyQt iports
from PyQt5 import QtWidgets

def start():
    app = QtWidgets.QApplication(sys.argv)

    #logger.info("------------------------------------------ Started ------------------------------------------\n")

    # fonctions.update_vars() done in MyPompierGLX
    #mySW = Ui_MyPompierGLX()
    #mySW.show()
    test = myMain.MyMain();
    test.show()
    sys.exit(app.exec_())



if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    logger.info("------------------------------------------ Started ------------------------------------------\n")

    fonctions.update_vars()
    mySW = Ui_MyPompierGLX()
    mySW.show()
    sys.exit(app.exec_())


"""
    Firefox_Sync Alpha
    Copyright (C) 2018  Sydney Rodolphe Torcuato Gems

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
