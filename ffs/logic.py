import paramiko
import re
import subprocess

from ffs import logger

def check_remote_conf(remote_conf):
    return remote_conf["host"] and remote_conf["user"] and remote_conf["path"]


def get_remote_files(remote_conf):
    """
    return: dict with distant files OR string describing the exception if any.
    """
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    try:
        client.connect(remote_conf["host"], username=remote_conf["user"], timeout=5)
        stdin, stdout, stderr = client.exec_command('ls ' + remote_conf["path"] + "/" + remote_conf["profile"])

        file_dict: dict = {}
        for line in stdout:
            l = re.search('\d{4}.\d{2}.\d{2}-\d{2}:\d{2}:\d{2,10}', line)
            if l:
                lg = l.group(0)
                year = re.search('\d{4}', lg).group(0)
                month = re.search('(?<=\d{4}.)\d{2}', lg).group(0)
                day = re.search('(?<=\d{4}.\d{2}.)\d{2}', lg).group(0)

                if not year in file_dict:
                    file_dict[year] = {}
                if not month in file_dict[year]:
                    file_dict[year][month] = {}
                if not day in file_dict[year][month]:
                    file_dict[year][month][day] = {}

                if l:
                    file_dict[year][month][day][lg] = line.replace("\n", "")  # with ls an \n is added, so remove it

    except Exception as e:
        logger.exception("Failed to get distant files")
        file_dict = e

    client.close()

    return file_dict


def get_remote_file(remote_conf, file_name):
    """
    get remote file and copy it to the selected firefox profile
    """
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.connect(remote_conf["host"], username=remote_conf["user"])
    sftp_client =client.open_sftp()

    sftp_client.get(remote_conf["path"] + "/" + remote_conf["profile"], "/tmp")
    

    sftp_client.close()
    client.close()

    return


def restore_remote(remote_conf, file_name):
    """
    Use selected file as latest
    """
    exit_msg = ""
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    try:
        client.connect(remote_conf["host"], username=remote_conf["user"], timeout=5)
        remote_path = "/" + remote_conf["path"] + "/" + remote_conf["profile"] + '/'
        remote_default_file = remote_path + "sessionstore.jsonlz4"
        stdin, stdout, stderr = client.exec_command('cp ' + remote_path + file_name + " " + remote_default_file)

        exit_msg = stdout
        for line in stdout:
            print(line)
        print(exit_msg)
    except Exception as e:
        exit_msg = e
        logger.exception("Failed to restore distant file")

    client.close()
    
    return exit_msg


def is_ff_running():
    return int(subprocess.check_output('ps ux | grep firefox | grep -v grep | wc -l', shell=True))
